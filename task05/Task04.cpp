#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <climits>
#include <cstring>
#include <omp.h>
#include <chrono>
#include <queue>
#include <mpi.h>
#include <algorithm>
#include <iterator>

#define LENGTH 256

struct Matrix {
	int size;
	int* values;

	int at(int x, int y) {
		if (y * size + x < size * size) {
			return values[y * size + x];
		} else {
			return -1;
		}
	}
};

struct State {
	bool config[40];
	int depth;
};

// --- Global Values ---
int min_weight_global = INT_MAX;
int solution_num_global = 0;
Matrix graph;
int a_global = 5;
bool solution_g[40];
int threadLimit = 16;
int stateLimit;

enum TAGS {
	TAG_WORK,
	TAG_DONE,
	TAG_TERMINATE,
};
// ---------------------

void printArray(const bool* config) {
	for (int i = 0; i < graph.size; ++i)
		std::cout << config[i];
	std::cout << std::endl;
}

void BFSQFill(std::deque<State>& stateQ) {
	int n = 0;

	State state, stateL, stateR;
	while (true) {
		state = stateQ.back();
		stateQ.pop_back();

		if (state.depth >= graph.size) {
			break;
		}

		memcpy(stateL.config, state.config, graph.size);
		stateL.depth = state.depth + 1;
		stateL.config[state.depth] = 0;

		memcpy(stateR.config, state.config, graph.size);
		stateR.depth = state.depth + 1;
		stateR.config[state.depth] = 1;

		stateQ.push_front(stateL);
		stateQ.push_front(stateR);

		n = n + 1;
		if (n > stateLimit) {
			break;
		}
	}
}

int calculatePotentialWeight(const State& state) {
	int min0 = 0, min1 = 0, sum = 0;

	for (int i = state.depth; i < graph.size; ++i) {	// For each node in unknown parf of the array
		min0 = 0, min1 = 0;
		for (int x = 0; x < state.depth; ++x) {			// go through each node in the known part of the array
			if (state.config[x] == 1) {					// and pretend, that there is 0 in the unknown part, so i am looking for connections with 1.
				min0 += graph.at(i, x);
			} 

			if (state.config[x] == 0) {					// and pretend, that there is 1 in the unknown node, so i am looking for connections with 0.
				min1 += graph.at(i, x);
			}
		}	
		if (min0 < min1) {
			sum += min0;
		} else {
			sum += min1;
		}
	}

	return sum;
}

void R(const State& state) {
	// --- Brute Force ---
	int currentWeight = 0, aCount = 0, aInverseCount = 0;
	for (int x = 0; x < state.depth; ++x) {
		if (state.config[x] == 0) {
			++aCount;
			aInverseCount = 0;
			for (int y = 0; y < state.depth; ++y) {
				if (state.config[y] == 1) {
					++aInverseCount;
					if (graph.at(x, y)) {
						currentWeight += graph.at(x, y);
					}
				}
			}
		}
	}
	// -------------------

	// --- Solution Storing ---
	if (state.depth == graph.size) { // Is list of a binary tree.
		if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
			#pragma omp critical
			{
			if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
				if (currentWeight == min_weight_global) {
					++solution_num_global;
				} else {
					solution_num_global = 0;
					++solution_num_global;
				}
				std::memcpy(solution_g, state.config, graph.size);
				min_weight_global = currentWeight;
			}
			}
		}
		return;
	}
	// ------------------------

	// --- Branch and Bounds ---
	if (currentWeight > min_weight_global) {
		return;
	}

	// Cut of the branch in case of bigger a.
	if (aCount > a_global && aInverseCount > a_global) {
		return;
	}
	 
	int potentialWeight = calculatePotentialWeight(state);
	if ((potentialWeight + currentWeight) > min_weight_global) {
		return;
	}
	// -------------------------

	// --- Left Branch ---
	State stateL;
	std::memcpy(stateL.config, state.config, graph.size);
	stateL.config[state.depth] = 0;
	stateL.depth = state.depth + 1;
	R(stateL);
	// -------------------

	// --- Right Branch ---
	State stateR;
	std::memcpy(stateR.config, state.config, graph.size);
	stateR.config[state.depth] = 1;
	stateR.depth = state.depth + 1;
	R(stateR);
	// --------------------
}

int main(int argc, char *argv[]) {
	std::ifstream inputFileStream;
	std::string inputFileName = argv[1];
	a_global = std::stoi(argv[2]);
	int threadNum = std::stoi(argv[3]);
	// std::cout << argv[1] << " a=" << argv[2] << std::endl;

	inputFileStream.open(inputFileName, std::ios::in);
	if (!inputFileStream.is_open() || !inputFileStream) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	inputFileStream >> graph.size;
	graph.values = new int[graph.size * graph.size];
	for (int i = 0; i < graph.size * graph.size; ++i) {
		inputFileStream >> graph.values[i];
	}
	inputFileStream.close();
	// -----------------

	// --- MPI ---
	int my_rank = -1;
	int pNum = -1;
	MPI_Status status;
	State bestState;
	bestState.depth = INT_MAX;
	solution_num_global = 0;
	std::vector<State> solutions;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &pNum);

	if (my_rank == 0)
		std::cout << "MPI parallelism " << inputFileName << " " << a_global << " " << pNum << " " << threadNum << std::endl;

	stateLimit = pNum * 16;

	auto start = std::chrono::steady_clock::now();
	if (my_rank == 0) { // Master
		// printf("MASTER\n");

		// Initial expansion - start
		State initialState;
		initialState.depth = 1;
		for (int i = 0; i < 40; ++i) {
			initialState.config[i] = 0;
		}

		std::deque<State> stateQ;
		stateQ.push_front(initialState);
		BFSQFill(stateQ);
		// printf("Size of queue = %ld\n", stateQ.size());
		// Initial expansion - end

		for (int dest = 1; dest < pNum; ++dest) {
			if (!stateQ.empty()) {
				State state = stateQ.front();
				stateQ.pop_front();

				// printf("Master sending initial work to slave num. %d: %d ", dest, state.depth);
				// printArray(state.config);

				MPI_Ssend(&state, sizeof(State), MPI_PACKED, dest, TAG_WORK, MPI_COMM_WORLD);
			}
		}

		int workingSlaves = pNum - 1;
		bool wasPresent = false;

		while (workingSlaves) {
			State state;
			MPI_Recv(&state, sizeof(State), MPI_PACKED, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG == TAG_DONE) {
				// TODO calculate best solution
				// printf("Master received solution: %d\n", state.depth);
				if (state.depth <= bestState.depth) {
					if (state.depth == bestState.depth) { // minimal weight is stored in depth
						for (size_t i = 0; i < solutions.size(); ++i) {
							if (std::equal(std::begin(state.config), std::end(state.config), std::begin(solutions[i].config))) {
								wasPresent = true;
								break;
							}
						}

						if (!wasPresent) {
							solutions.push_back(state);
						}

						wasPresent = false;

					} else {
						solutions.clear();
						solutions.push_back(state);
						bestState.depth = state.depth;
					}
				}
			}

			if (stateQ.empty()) {
				MPI_Ssend(&state, sizeof(State), MPI_PACKED, status.MPI_SOURCE, TAG_TERMINATE, MPI_COMM_WORLD);
				--workingSlaves;
			} else {
				State state = stateQ.front();
				stateQ.pop_front();

				// printf("Master sending work to slave num. %d: %d ", status.MPI_SOURCE, state.depth);
				// printArray(state.config);

				MPI_Ssend(&state, sizeof(State), MPI_PACKED, status.MPI_SOURCE, TAG_WORK, MPI_COMM_WORLD);
			}
		}

	} else { // SLAVE
		while (true) {
			State stateMsg;
			MPI_Recv(&stateMsg, sizeof(State), MPI_PACKED, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG == TAG_TERMINATE) {
				break;
			} else if (status.MPI_TAG == TAG_WORK) {
				// printf("%d. slave received message: %d ", my_rank, stateMsg.depth);
				// printArray(stateMsg.config);

				std::deque<State> q;
				q.push_front(stateMsg);
				BFSQFill(q);

				std::vector<State> states;
				states.reserve(q.size());
				for (size_t i = 0; i < q.size(); ++i) {
					states.push_back(q.at(i));
				}

				#pragma omp parallel for schedule (dynamic, 1) num_threads(threadNum)
				for (size_t i = 0; i < states.size(); ++i) {
					State state = states[i];
					R(state);
				}

				// printf("%d. slave result is: %d\n", my_rank, min_weight_global);

				memcpy(stateMsg.config, solution_g, 40);
				stateMsg.depth = min_weight_global;
				MPI_Ssend(&stateMsg, sizeof(State), MPI_PACKED, 0, TAG_DONE, MPI_COMM_WORLD);
			}
		}		
	}
	// -----------
	
	// --- Output ---
	if (my_rank == 0) {
		fflush(stdout);
		std::cout << bestState.depth << std::endl;
		std::cout << solutions.size() << std::endl;
		printArray(solutions[0].config);

		auto end = std::chrono::steady_clock::now();
		std::chrono::duration<double> timer = end - start;
		std::cout << timer.count() << std::endl;
	}
	// --------------

	// --- Clean Up ---
	delete[] graph.values;
	MPI_Finalize();

 	return 0;
}
