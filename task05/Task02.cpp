#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <climits>
#include <cstring>
#include <omp.h>
#include <chrono>

struct Matrix {
	int size;
	int* values;

	int at(int x, int y) {
		if (y * size + x < size * size) {
			return values[y * size + x];
		} else {
			return -1;
		}
	}
};

// --- Global Values ---
int min_weight_global = INT_MAX;
int solution_num_global = 0;
Matrix graph;
int a_global = 5;
int RCounter = 0;
std::vector<bool*> solutions;
int threadLimit = 16;
// ---------------------

void printArray(bool* config) {
	for (int i = 0; i < graph.size; ++i)
		std::cout << config[i];
	std::cout << std::endl;
}

int calculatePotentialWeight(const int index, const bool* config_) {
	int min0 = 0, min1 = 0, sum = 0;

	for (int i = index; i < graph.size; ++i) {  // For each node in unknown parf of the array
		min0 = 0, min1 = 0;
		for (int x = 0; x < index; ++x) {       // go through each node in the known part of the array
			if (config_[x] == 1) {              // and pretend, that there is 0 in the unknown part, so i am looking for connections with 1.
				min0 += graph.at(i, x);
			} 

			if (config_[x] == 0) {              // and pretend, that there is 1 in the unknown node, so i am looking for connections with 0.
				min1 += graph.at(i, x);
			}
		}	
		if (min0 < min1) {
			sum += min0;
		} else {
			sum += min1;
		}
	}

	return sum;
}

void R(int index, bool* config) {
	// --- Brute Force ---
	int currentWeight = 0, aCount = 0, aInverseCount = 0;
	for (int x = 0; x < index; ++x) {
		if (config[x] == 0) {
			++aCount;
			aInverseCount = 0;
			for (int y = 0; y < index; ++y) {
				if (config[y] == 1) {
					++aInverseCount;
					if (graph.at(x, y)) {
						currentWeight += graph.at(x, y);
					}
				}
			}
		}
	}
	// -------------------

	// --- Solution Storing ---
	if (index == graph.size) { // Is list of a binary tree.
		if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
			// TODO kriticka sekce
			#pragma omp critical
			{			
				if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
					bool* tmpcnfg = new bool[graph.size];
					std::memcpy(tmpcnfg, config, graph.size);

					if (currentWeight == min_weight_global) {
						++solution_num_global;
						solutions.push_back(tmpcnfg);
					} else {
						solution_num_global = 0;
						++solution_num_global;

						for (size_t i = 0; i < solutions.size(); ++i) {
							delete[] solutions[i];
						}

						solutions.clear();
						solutions.push_back(tmpcnfg);
					}
					min_weight_global = currentWeight;
				}
			}
		}
		return;
	}
	// ------------------------

	// --- Branch and Bounds ---
	if (currentWeight > min_weight_global) {
		return;
	}

	// Cut of the branch in case of bigger a.
	if (aCount > a_global && aInverseCount > a_global) {
		return;
	}
	 
	int potentialWeight = calculatePotentialWeight(index, config);
	if ((potentialWeight + currentWeight) > min_weight_global) {
		return;
	}
	// -------------------------

	// --- Left Branch ---
	bool configL[graph.size];
	std::memcpy(configL, config, graph.size);
	configL[index] = 0;
	#pragma omp task if (index < threadLimit)
	{
	R(index + 1, configL);
	}
	// -------------------

	// --- Right Branch ---
	bool configR[graph.size];
	std::memcpy(configR, config, graph.size);
	configR[index] = 1;
	#pragma omp task if (index < threadLimit)
	{
	R(index + 1, configR);
	}
	// --------------------
}

int main(int argc, char *argv[]) {
	std::ifstream inputFileStream;
	std::string inputFileName = argv[1];
	a_global = std::stoi(argv[2]);
	int threadNum = std::stoi(argv[3]);

	std::cout << "Task parallelism " <<  inputFileName << " " << a_global << " " << threadNum << std::endl;


	inputFileStream.open(inputFileName, std::ios::in);
	if (!inputFileStream.is_open() || !inputFileStream) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	inputFileStream >> graph.size;
	graph.values = new int[graph.size * graph.size];
	// graph.values[graph.size * graph.size];
	for (int i = 0; i < graph.size * graph.size; ++i) {
		inputFileStream >> graph.values[i];
	}
	inputFileStream.close();
	// -----------------

	// --- Not so Sequential Algorithm ---
	bool* current = new bool[graph.size];
	// bool current[graph.size];
	for (int i = 0; i < graph.size; ++i) {
		current[i] = 0;
	}

		// TODO add chrono timer
	auto start = std::chrono::steady_clock::now();
		#pragma omp parallel num_threads(threadNum)
		{
			#pragma omp single 
			{
				R(1, current);
			}
		}
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> timer = end - start;
	
	// ----------------------------

	// --- Output ---
	std::cout << min_weight_global << std::endl;
	std::cout << solution_num_global << std::endl;
	for (size_t i = 0; i < 1; ++i) {
		printArray(solutions[i]);
	}
	std::cout << timer.count() << std::endl;
	// --------------

	// --- Clean Up ---
	delete[] graph.values;
	delete[] current;
	for (size_t i = 0; i < solutions.size(); ++i) {
		delete[] solutions[i];
	}

 	return 0;
}
