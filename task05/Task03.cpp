#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <climits>
#include <cstring>
#include <omp.h>
#include <chrono>
#include <queue>

struct Matrix {
	int size;
	int* values;

	int at(int x, int y) {
		if (y * size + x < size * size) {
			return values[y * size + x];
		} else {
			return -1;
		}
	}
};

struct State {
	bool config[40];
	int depth;
};

// --- Global Values ---
int min_weight_global = INT_MAX;
int solution_num_global = 0;
Matrix graph;
int a_global = 5;
bool solution_g[40];
int threadLimit = 16;
std::deque<State> stateQ;
int stateLimit = 500;
// ---------------------

void printArray(const bool* config) {
	for (int i = 0; i < graph.size; ++i)
		std::cout << config[i];
	std::cout << std::endl;
}

void BFSQFill() {
	int n = 0;

	State state, stateL, stateR;
	while (true) {
		state = stateQ.back();
		stateQ.pop_back();

		memcpy(stateL.config, state.config, graph.size);
		stateL.depth = state.depth + 1;
		stateL.config[state.depth] = 0;

		memcpy(stateR.config, state.config, graph.size);
		stateR.depth = state.depth + 1;
		stateR.config[state.depth] = 1;

		stateQ.push_front(stateL);
		stateQ.push_front(stateR);

		n = n + 2;
		if (n > stateLimit) {
			break;
		}
	}
}

int calculatePotentialWeight(const State& state) {
	int min0 = 0, min1 = 0, sum = 0;

	for (int i = state.depth; i < graph.size; ++i) {	// For each node in unknown parf of the array
		min0 = 0, min1 = 0;
		for (int x = 0; x < state.depth; ++x) {			// go through each node in the known part of the array
			if (state.config[x] == 1) {					// and pretend, that there is 0 in the unknown part, so i am looking for connections with 1.
				min0 += graph.at(i, x);
			} 

			if (state.config[x] == 0) {					// and pretend, that there is 1 in the unknown node, so i am looking for connections with 0.
				min1 += graph.at(i, x);
			}
		}	
		if (min0 < min1) {
			sum += min0;
		} else {
			sum += min1;
		}
	}

	return sum;
}

void R(const State& state) {
	// --- Brute Force ---
	int currentWeight = 0, aCount = 0, aInverseCount = 0;
	for (int x = 0; x < state.depth; ++x) {
		if (state.config[x] == 0) {
			++aCount;
			aInverseCount = 0;
			for (int y = 0; y < state.depth; ++y) {
				if (state.config[y] == 1) {
					++aInverseCount;
					if (graph.at(x, y)) {
						currentWeight += graph.at(x, y);
					}
				}
			}
		}
	}
	// -------------------

	// --- Solution Storing ---
	if (state.depth == graph.size) { // Is list of a binary tree.
		if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
			#pragma omp critical
			{			
				if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
					if (currentWeight == min_weight_global) {
						++solution_num_global;
					} else {
						solution_num_global = 0;
						++solution_num_global;
					}
					std::memcpy(solution_g, state.config, graph.size);
					min_weight_global = currentWeight;
				}
			}
		}
		return;
	}
	// ------------------------

	// --- Branch and Bounds ---
	if (currentWeight > min_weight_global) {
		return;
	}

	// Cut of the branch in case of bigger a.
	if (aCount > a_global && aInverseCount > a_global) {
		return;
	}
	 
	int potentialWeight = calculatePotentialWeight(state);
	if ((potentialWeight + currentWeight) > min_weight_global) {
		return;
	}
	// -------------------------

	// --- Left Branch ---
	State stateL;
	std::memcpy(stateL.config, state.config, graph.size);
	stateL.config[state.depth] = 0;
	stateL.depth = state.depth + 1;
	R(stateL);
	// -------------------

	// --- Right Branch ---
	State stateR;
	std::memcpy(stateR.config, state.config, graph.size);
	stateR.config[state.depth] = 1;
	stateR.depth = state.depth + 1;
	R(stateR);
	// --------------------
}

int main(int argc, char *argv[]) {
	std::ifstream inputFileStream;
	std::string inputFileName = argv[1];
	a_global = std::stoi(argv[2]);
	int threadNum = std::stoi(argv[3]);
	// std::cout << argv[1] << " a=" << argv[2] << std::endl;
	std::cout << "Data parallelism " << inputFileName << " " << a_global << " " << threadNum << std::endl;

	inputFileStream.open(inputFileName, std::ios::in);
	if (!inputFileStream.is_open() || !inputFileStream) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	inputFileStream >> graph.size;
	graph.values = new int[graph.size * graph.size];
	for (int i = 0; i < graph.size * graph.size; ++i) {
		inputFileStream >> graph.values[i];
	}
	inputFileStream.close();
	// -----------------

	// --- Data Parallelism ---
	State initialState;
	initialState.depth = 1;
	for (int i = 0; i < 40; ++i) {
		initialState.config[i] = 0;
	}

	stateQ.push_front(initialState);
	BFSQFill();

	std::vector<State> states;
	states.reserve(stateQ.size());
	for (size_t i = 0; i < stateQ.size(); ++i) {
		states.push_back(stateQ.at(i));
	}

	auto start = std::chrono::steady_clock::now();
	#pragma omp parallel for schedule (dynamic, 1) num_threads(threadNum)
	for (size_t i = 0; i < stateQ.size(); ++i) {
		State state = states[i];
		R(state);
	}
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> timer = end - start;
	// ----------------------------

	// --- Output ---
	std::cout << min_weight_global << std::endl;
	std::cout << solution_num_global << std::endl;
	printArray(solution_g);
	std::cout << timer.count() << std::endl;
	// --------------

	// --- Clean Up ---
	delete[] graph.values;

 	return 0;
}
