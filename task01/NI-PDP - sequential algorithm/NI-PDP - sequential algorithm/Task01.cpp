#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <climits>
#include <cstring>

struct Matrix {
	int size;
	int* values;

	int at(int x, int y) {
		if (y * size + x < size * size) {
			return values[y * size + x];
		} else {
			return -1;
		}
	}
};

// --- Global Values ---
int min_weight_global = INT_MAX;
int solution_num_global = 0;
Matrix graph;
int a_global = 5;
int RCounter = 0;
std::vector<bool*> solutions;
// ---------------------

void printArray(bool* config) {
	for (int i = 0; i < graph.size; ++i)
		std::cout << config[i];
	std::cout << std::endl;
}

int calculatePotentialWeight(const int index, bool* config_) {
	int min0 = 0, min1 = 0, sum = 0;

	for (int i = index; i < graph.size; ++i) {  // For each node in unknown parf of the array
		min0 = 0, min1 = 0;
		for (int x = 0; x < index; ++x) {       // go through each node in the known part of the array
			if (config_[x] == 1) {              // and pretend, that there is 0 in the unknown part, so i am looking for connections with 1.
				min0 += graph.at(i, x);
			} 

			if (config_[x] == 0) {              // and pretend, that there is 1 in the unknown node, so i am looking for connections with 0.
				min1 += graph.at(i, x);
			}
		}	
		if (min0 < min1) {
			sum += min0;
		} else {
			sum += min1;
		}
	}

	return sum;
}

void R(int index, bool* config) {
	// --- Brute Force ---
	int currentWeight = 0, aCount = 0, aInverseCount = 0;
	for (int x = 0; x < index; ++x) {
		if (config[x] == 0) {
			++aCount;
			aInverseCount = 0;
			for (int y = 0; y < index; ++y) {
				if (config[y] == 1) {
					++aInverseCount;
					if (graph.at(x, y)) {
						currentWeight += graph.at(x, y);
					}
				}
			}
		}
	}
	// -------------------

	// --- Solution Storing ---
	if (index == graph.size) { // Is list of a binary tree.
		if ((aCount == a_global  || aInverseCount == a_global) && currentWeight > 0 && currentWeight <= min_weight_global) {
			bool* tmpcnfg = new bool[graph.size];
			memcpy(tmpcnfg, config, graph.size);

			if (currentWeight == min_weight_global) {
				++solution_num_global;
				solutions.push_back(tmpcnfg);
			} else {
				solution_num_global = 0;
				++solution_num_global;

				for (size_t i = 0; i < solutions.size(); ++i) {
					delete[] solutions[i];
				}
				solutions.clear();
				solutions.push_back(tmpcnfg);
			}
			min_weight_global = currentWeight;
		}

		return;
	}
	// ------------------------

	// --- Branch and Bounds ---
	if (currentWeight > min_weight_global) {
		return;
	}

	// Cut of the branch in case of bigger a.
	if (aCount > a_global && aInverseCount > a_global) {
		return;
	}
	 
	int potentialWeight = calculatePotentialWeight(index, config);
	if ((potentialWeight + currentWeight) > min_weight_global) {
		return;
	}
	// -------------------------

	// --- Left Branch ---
	config[index] = 0;
	R(index + 1, config);
	// -------------------

	// --- Right Branch ---
	config[index] = 1;
	R(index + 1, config);
	// --------------------
}

int main() {
	std::cout << "Hello NI-PDP Sequential Algorithm part." << std::endl;

	std::ifstream inputFileStream;
	a_global = 5;
	std::string inputFileName = "graf_mhr//graf_15_14.txt";

	inputFileStream.open(inputFileName, std::ios::in);
	if (!inputFileStream.is_open() || !inputFileStream) {
		std::cout << "File does not exist or is corrupted." << std::endl;
		return false;
	}

	inputFileStream >> graph.size;
	graph.values = new int[graph.size * graph.size];
	for (size_t i = 0; i < graph.size * graph.size; ++i) {
		inputFileStream >> graph.values[i];
	}
	inputFileStream.close();
	// -----------------

	// --- Sequential Algorithm ---
	bool* current = new bool[graph.size];
	for (int i = 0; i < graph.size; ++i) {
		current[i] = 0;
	}
	R(1, current);
	// ----------------------------

	// --- Output ---
	std::cout << "Weight = " << min_weight_global << std::endl;
	std::cout << "#Solutions = " << solution_num_global << std::endl;
	for (size_t i = 0; i < solutions.size(); ++i) {
		printArray(solutions[i]);
	}
	// --------------

	// --- Clean Up ---
	delete[] graph.values;
	delete[] current;
	for (size_t i = 0; i < solutions.size(); ++i) {
		delete[] solutions[i];
	}

 	return 0;
}
